<?php

namespace App\Http\Controllers;

use App\Supplier;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Image;
use File;  

class SupplierController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');
    }
   

    public function supplieradd()
    {
        return view('admin.supplier.add');
    }

    public function supplierinsert(Request $request)
    {    
           
 $request->validate([         
            'name' => 'required|max:30',
            'email' => 'required|email',
            'address' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,bmp',
            'mobile' => 'required|numeric',
            'position' => 'required',
            'fax' => 'required',
            'opening_balance' => 'required',
            'contact_person' => 'required',            
            'bank_name' => 'required',
            'account_number' => 'required',
            'account_name' => 'required',
            'branch' => 'required',     

     ]);



            $data=array();
            $data['name']=$request->name;
            $data['email']=$request->email;
            $data['address']=$request->address;
            $data['position']=$request->position;
            $data['mobile']=$request->mobile;          
            $data['fax']=$request->fax;
            $data['contact_person']=$request->contact_person;
            $data['mobile_company']=$request->mobile_company;
            $data['bank_name']=$request->bank_name;
            $data['account_name']=$request->account_name;
            $data['account_number']=$request->account_number;
            $data['branch']=$request->branch;
            $data['opening_balance']=$request->opening_balance;
             $image=$request->file('image');
             if ($image) {
            $image_name= hexdec(uniqid());           
            $filename=$image_name.".".$image->getClientOriginalExtension();
            Image::make($image)->resize(200, 200)->save('public/panel/assets/images/supplier/'.$filename);
            $data['image']=$filename;

            $suplierinsert=DB::table('suppliers')
                        ->insertGetId($data);
            $day=date('d');
            $month=date('Y');
            $supplier_id='S-'.$suplierinsert.$day.$month; 
            $customer=DB::table('suppliers')->where('id',$suplierinsert)->update(['supplier_id' => $supplier_id]);
             $notification=array(
                 'messege'=>'Successfully Supplier  Inserted ',
                 'alert-type'=>'success'
                );
            return Redirect()->to('/admin/list/supplier')->with($notification);                  
       }else{

           $suplierinsert=DB::table('suppliers')
                    ->insertGetId($data);
            $day=date('d');
            $month=date('Y');
            $supplier_id='C-'.$suplierinsert.$day.$month; 
            $customer=DB::table('suppliers')->where('id',$suplierinsert)->update(['supplier_id' => $supplier_id]);
             $notification=array(
                 'messege'=>'Successfully Supplier Inserted ',
                 'alert-type'=>'success'
                );
            return Redirect()->route('list.customer')->with($notification);        

       }
    }

  
    public function supplierlist()
    {
         $suppliers=Supplier::all();
        return view('admin.supplier.view',compact('suppliers'));   
    }

    public function supplierdelete($sup_id)
    {
         $post=DB::table('suppliers')->where('id',$sup_id)->first();
          $image=$post->image;
          $delete=DB::table('suppliers')->where('id',$sup_id)->delete();
           if ($delete) {
              unlink('public/panel/assets/images/supplier/'.$image);
          $notification=array(
            'messege'=>'Supplier Delete Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);

    
    }
  }

  public function supplieredit($sup_id)
    {
     
      $suplieredit= DB::table('suppliers')->where('id',$sup_id)->first();
     return view('admin.supplier.edit',compact('suplieredit'));

  }

  public function supplierupdate(Request $request)
  {


             $old=$request->oldpic;
             $id=$request->id;
             $supplierupdate=Supplier::where('id',$id)->update([

                    'name' => $request->name,
                    'email' => $request->email,
                    'address' => $request->address,
                    'position' => $request->position,
                    'mobile' => $request->mobile,
                    'fax' => $request->fax,
                    'contact_person' => $request->contact_person,
                    'mobile_company' => $request->mobile_company,
                    'bank_name' => $request->bank_name,
                    'account_name' => $request->account_name,
                    'account_number' => $request->account_number,
                    'branch' => $request->branch,
                    'opening_balance' => $request->opening_balance,
                 
             ]);
             if($request->hasFile('image')) 
            {
            unlink('public/panel/assets/images/supplier/'.$old);          
            $photo=$request->image; 
            $filename=hexdec(uniqid()).".".$photo->getClientOriginalExtension();
            Image::make($photo)->resize(200, 200)->save('public/panel/assets/images/supplier/'.$filename);
              
             Supplier::find($id)->update([
                'image'=> $filename               
              ]);
              
                $notification=array(
               'messege'=>'Supplier Updated Successfully',
               'alert-type'=>'success'
                  );
               return Redirect()->to('/admin/list/supplier')->with($notification);
               
            
             }
      
  }

    public function suppliersingleview($sup_id)
    {

         $single_view=Supplier::where('id',$sup_id)->first();             
               return view('admin.supplier.singleview',compact('single_view'));
        
    }
}
