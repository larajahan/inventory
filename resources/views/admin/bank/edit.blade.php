@extends('admin.admin_layouts')
@section('admin_content')
<div class="content_wrapper">
	<!--middle content wrapper-->
	<!-- page content -->
	<div class="middle_content_wrapper ">
		<section class="page_content">
			<div class="panel mb-0">
				<div class="panel_header w-75 offset-1">
					<div class="panel_title">
						<span class="panel_icon"><i class="fas fa-border-all"></i></span><span>Edit Bank</span>
					</div>
				</div>
				<div class="panel_body w-75 offset-1">
					<div class="row">
						<div class="col-md-12 col-xs-12 ">
							
							<div class="card">
								<h5 class="card-header">Bank Form</h5>
								<div class="card-body">
									@if ($errors->all())
									<div class="alert alert-danger">
										@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
										@endforeach
									</div>
									@endif
									<form action="{{route('admin.update.bank')}}" method="post">
										@csrf
										<div class="form-row">
											<div class="col-md-6 col-xs-12">
												<div class="form-group">
													<label>Bank Name</label>
													<input type="hidden" class="form-control"  name="id" value="{{ $bankedit->id}}" >
													<input type="text" class="form-control "  name="bank" value="{{ $bankedit->bank_name}}">
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-primary">Update Bank</button>
									</form>
									
								</div>
							</div>
							
							
						</div>
						
					</div>
					</div> <!--/ panel body -->
					</div><!--/ panel -->
				</section>
				<!--/ page content -->
				<!-- start code here... -->
				</div><!--/middle content wrapper-->
				</div><!--/ content wrapper -->
				@endsection