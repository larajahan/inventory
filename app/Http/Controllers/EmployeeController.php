<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use Image;
use File;
use App\Employee;

class EmployeeController extends Controller
{
    public function __construct()
   {
     return $this->middleware('auth');
   }

    public function index()
    {  
      $employees=Employee::all();
    	return view('employee.index',compact('employees'));
    }

     public function create()
    {
    	return view('employee.create');
    }

    public function store(Request $request )
    {
    	$request->validate([
        'name' => 'required|max:30',
        'email' => 'required|email',
        'address' => 'required',
        'image' => 'required|mimes:jpeg,png,jpg,bmp',
        'mobile' => 'required|numeric',
        'fax' => 'required',
        'contact_person' => 'required',
        'position' => 'required',
        'company_person' => 'required',
        'bank_name' => 'required',     
        'account_number' => 'required',     
        'account_name' => 'required',     
        'branch' => 'required',        
        'salary' => 'required',     
        'perday' => 'required',     
         ]); 

       $data=array();
       $data['user_id']=Auth::id();
       $data['name']=$request->name;
       $data['email']=$request->email;
       $data['address']=$request->address;
       $data['mobile']=$request->mobile;
       $data['fax']=$request->fax;
       $data['contact_person']=$request->contact_person;
       $data['position']=$request->position;
       $data['company_person']=$request->company_person;
       $data['bank_name']=$request->bank_name;
       $data['account_number']=$request->account_number;
       $data['account_name']=$request->account_name;
       $data['branch']=$request->branch;
       $data['salary']=$request->salary;
       $data['perday']=$request->perday;
       $image=$request->file('image');
       if ($image) {
            $image_name= hexdec(uniqid());
            // $ext=strtolower($image->getClientOriginalExtension());
            // $image_full_name=$image_name.'.'.$ext;
            // $upload_path='public/panel/customer/';
            // $image_url=$upload_path.$image_full_name;
            // $success=$image->move($upload_path,$image_full_name);
            $filename=$image_name.".".$image->getClientOriginalExtension();
            Image::make($image)->resize(200, 200)->save('public/panel/employee/'.$filename);
            $data['image']=$filename;

            $empyinsert=DB::table('employees')
                        ->insertGetId($data);
            $day=date('d');
            $month=date('Y');
            $employee_id='S-'.$empyinsert.$day.$month; 
            $employee=DB::table('employees')->where('id',$empyinsert)->update(['employee_id' => $employee_id]);
             $notification=array(
                 'messege'=>'Successfully Employee Inserted ',
                 'alert-type'=>'success'
                );
            return Redirect()->back()->with($notification);                  
       }else{

           $empyinsert=DB::table('employees')
                    ->insertGetId($data);
            $day=date('d');
            $month=date('Y');
            $employee_id='C-'.$empyinsert.$day.$month; 
            $employee=DB::table('employees')->where('id',$empyinsert)->update(['employee_id' => $employee_id]);
             $notification=array(
                 'messege'=>'Successfully Customer Inserted ',
                 'alert-type'=>'success'
                );
            return Redirect()->back()->with($notification);        

       }
    }

 //Delete
  public function delete($id)   {
      
          $employee=DB::table('employees')->where('id',$id)->first();
          $image=$employee->image;
          $delete=DB::table('employees')->where('id',$id)->delete();

           if ($delete) {
              unlink('public/panel/employee/'.$image);
          $notification=array(
            'messege'=>'Employee Delete Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
     }else{
         $notification=array(
            'messege'=>'Failed!',
            'alert-type'=>'error'
             );
           return Redirect()->back()->with($notification);
     }
   }

    public function view($id)
  {

     $employee=DB::table('employees')->where('id',$id)->first();
     return view('employee.view',compact('employee'));
  }

  public function edit($id)
    {
      $employee=DB::table('employees')->where('id',$id)->first();
      return view('employee.edit',compact('employee'));
    }


    public function update(Request $request)
    {
             $old=$request->oldpic;
             $id=$request->id;

            

            if($request->hasFile('image')) 
            {
            unlink('public/panel/employee/'.$old);
            $photo=$request->image; 
            $filename=hexdec(uniqid()).".".$photo->getClientOriginalExtension();
            Image::make($photo)->resize(200, 200)->save('public/panel/employee/'.$filename);
              
             Employee::where('id',$id)->update([
                'image'=> $filename               
              ]);
              
                $notification=array(
               'messege'=>'Employee Updated Successfully',
               'alert-type'=>'success'
                  );
               return Redirect()->back()->with($notification);
             }
             $employee=Employee::where('id',$id)->update([
             'name' => $request->name,
             'email' => $request->email,          
             'address' => $request->address,
             'mobile' => $request->mobile, 
             'fax' => $request->fax, 
             'contact_person' => $request->contact_person, 
             'position' => $request->position, 
             'bank_name' => $request->bank_name, 
             'account_number' => $request->account_number, 
             'account_name' => $request->account_name, 
             'branch' => $request->branch, 
             'salary' => $request->salary, 
             'perday' => $request->perday, 
            ]);
              $notification=array(
               'messege'=>'Employee Updated Successfully',
               'alert-type'=>'success'
                  );
               return Redirect()->back()->with($notification);

    }
    
}
