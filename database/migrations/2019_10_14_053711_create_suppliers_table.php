<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('supplier_id')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();            
            $table->text('address')->nullable();
            $table->string('position')->nullable();
            $table->integer('mobile')->nullable();
            $table->string('image')->nullable();
            $table->integer('fax')->nullable();
            $table->string('contact_person')->nullable();
            $table->integer('mobile_company')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('account_name')->nullable();
            $table->integer('account_number')->nullable();
            $table->string('branch')->nullable();
            $table->integer('opening_balance')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
