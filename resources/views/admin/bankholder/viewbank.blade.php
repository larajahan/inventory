@extends('admin.admin_layouts')
@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->
      <!-- panel -->
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>All Bank Holders</span>
          </div>
        </div>
        <div class="panel_body">        
            
            <div class="table-responsive">
              <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
                <thead>
                  <tr>                  
                    <th>Bank Name</th>
                    <th>Bank Holder </th>                   
                    <th>Account Number</th>
                    <th>Address</th>
                    <th>Known Person</th>
                    <th>Designation</th>           
                    <th>Mobile</th>
                    <th>Opening Balance</th>  
                    <th>Balance</th>  
                    <th>Action</th>                    
                  </tr>
                </thead>
                <tbody>
                  @foreach($bankholders as $row)
                  <tr>
                    <td >{{$row->bank_id}}</td>                 
                    <td>{{$row->bank_holder_name}}</td>
                    <td>{{$row->account_no}}</td>
                    <td>{{$row->address}}</td>                    
                    <td>{{$row->know_person}}</td>                    
                    <td>{{$row->designation}}</td>                    
                    <td>{{$row->mobile}}</td>                    
                    <td>{{$row->opening_blance}}</td>                    
                    <td>{{$row->balance}}</td>                    
                    <td>
                      <div class="btn-group" role="group">
                       
                        <a href="{{route('admin.single.bankholder',$row->id)}}"  class="btn btn-sm btn-secondary">View</a> 
                        <a href="{{route('admin.delete.bankholder',$row->id)}}" id="delete"  class="btn btn-sm btn-danger">Delete</a>
                        <a href="{{route('admin.edit.bankholder',$row->id)}}"  class="btn btn-sm btn-primary">Edit</a> 
                    
                      </div>                   
                    </td>   
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div> <!--/ panel body -->
            </div><!--/ panel -->
           
          </section>
          <!--/ page content -->
          <!-- start code here... -->
          </div><!--/middle content wrapper-->
          </div><!--/ content wrapper -->
          @endsection