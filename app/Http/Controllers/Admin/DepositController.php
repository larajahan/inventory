<?php

namespace App\Http\Controllers\Admin;
use App\Bank;
use App\Deposit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class DepositController extends Controller
{   
	  public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function adddeposit()
    {     $banks = Bank::all();    
                       
         return view('admin.deposit.addeposit',compact('banks'));
     }

    public function GetHolder($bankid)
    {
    	 $bank=DB::table('bankholders')->where('bank_id',$bankid)->select('id','bank_holder_name')->get();
       
    	return json_encode($bank);
    }

    public function GetDetailsHolder($hold_id)
    {
         $bank=DB::table('bankholders')->where('id',$hold_id)->select('id','account_no','address','balance')->first();
       
        return json_encode($bank);
    }
      public function GetPaybank($paybank_id)
    {
         $bank=DB::table('bankholders')->where('bank_id',$paybank_id)->select('id','bank_holder_name','balance')->get();
       
        return json_encode($bank);
    }  
    public function Pamentbank($payment_id)
    {
         $bank=DB::table('bankholders')->where('bank_id',$payment_id)->select('id','balance')->first();
       
        return json_encode($bank);
    }
     

     public function insertdeposit (Request $request)
     {     
           $data=array();
          $data['bank_id']=$request->bank_id;         
          $data['bank_holder_name']=$request->bank_holder_name;         
          $data['account_no']=$request->account_no;
          $data['address']=$request->address;
          $data['know_person']=$request->know_person;
          $data['designation']=$request->designation;
          $data['mobile']=$request->mobile;   
          $data['opening_blance']=$request->opening_blance;
          $data['balance']=$request->balance;
          $branchinsert=DB::table('bankholders')
                        ->insert($data);

     }
    
       

}
