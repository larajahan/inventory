<?php
Route::get('/', function(){
  return redirect()->to('/login');
});
//auth & user
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/password-change', 'HomeController@changePassword')->name('password.change');
Route::post('/password-update', 'HomeController@updatePassword')->name('password.update');
Route::get('/user/logout', 'HomeController@Logout')->name('user.logout');

//admin=======
Route::get('admin/home', 'AdminController@index');
Route::get('admin', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admin', 'Admin\LoginController@login');

// Password Reset Routes...
Route::get('admin/password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin/reset/password/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
Route::post('admin/update/reset', 'Admin\ResetPasswordController@reset')->name('admin.reset.update');
Route::get('/admin/Change/Password','AdminController@ChangePassword')->name('admin.password.change');
Route::post('/admin/password/update','AdminController@Update_pass')->name('admin.password.update');
Route::get('admin/logout', 'AdminController@logout')->name('admin.logout');

Route::get('admin/setting', 'SettingController@edit')->name('admin.setting.edit');
Route::post('admin/setting/update', 'SettingController@update')->name('admin.setting.update');

// Category Routes
Route::get('admin/index', 'CategoryController@index')->name('admin.category.index');
Route::get('admin/create', 'CategoryController@create')->name('admin.category.create');
Route::get('admin/edit/{id}', 'CategoryController@edit')->name('admin.category.edit');
Route::post('admin/store', 'CategoryController@store')->name('admin.category.store');
Route::post('admin/category/edit/{id}', 'CategoryController@update')->name('admin.category.update');
Route::get('admin/category/delete/{id}', 'CategoryController@delete')->name('admin.category.delete');

// Customer  Controller
//User  Customer  Controller




   Route::get('admin/setting', 'SettingController@edit')->name('admin.setting.edit');
   Route::post('admin/setting/update', 'SettingController@update')->name('admin.setting.update');



    // Category Routes
    Route::get('admin/index', 'CategoryController@index')->name('admin.category.index');
    Route::get('admin/create', 'CategoryController@create')->name('admin.category.create');
    Route::get('admin/edit/{id}', 'CategoryController@edit')->name('admin.category.edit');
    Route::post('admin/store', 'CategoryController@store')->name('admin.category.store');
    Route::post('admin/category/edit/{id}', 'CategoryController@update')->name('admin.category.update');
    Route::get('admin/category/delete/{id}', 'CategoryController@delete')->name('admin.category.delete');

   //Brand Routes
    Route::group(['prefix' => 'brands'], function(){
    Route::get('admin/index', 'Admin\BrandController@index')->name('admin.brands');
    Route::get('admin/create', 'Admin\BrandController@create')->name('admin.brand.create');
    Route::get('admin/edit/{id}', 'Admin\BrandController@edit')->name('admin.brand.edit');
    Route::post('admin/store', 'Admin\BrandController@store')->name('admin.brand.store');
    Route::post('admin/brand/edit/{id}', 'Admin\BrandController@update')->name('admin.brand.update');
    Route::get('admin/brand/delete/{id}', 'Admin\BrandController@delete')->name('admin.brand.delete');
});

    //Unity Routes
    Route::group(['prefix' => 'unities'], function(){
    Route::get('admin/index', 'Admin\UnitController@index')->name('admin.unities');
    Route::get('admin/create', 'Admin\UnitController@create')->name('admin.unity.create');
    Route::get('admin/edit/{id}', 'Admin\UnitController@edit')->name('admin.unity.edit');
    Route::post('admin/store', 'Admin\UnitController@store')->name('admin.unity.store');
    Route::post('admin/unity/edit/{id}', 'Admin\UnitController@update')->name('admin.unity.update');
    Route::get('admin/unity/delete/{id}', 'Admin\UnitController@delete')->name('admin.unity.delete');
});

      //Produtc Routes
Route::group(['prefix' => 'products'], function(){
 Route::get('admin/index', 'Admin\ProductController@index')->name('admin.products');
 Route::get('admin/create', 'Admin\ProductController@create')->name('admin.product.create');
 Route::get('admin/edit/{id}', 'Admin\ProductController@edit')->name('admin.product.edit');
 Route::post('admin/store', 'Admin\ProductController@store')->name('admin.product.store');
 Route::post('admin/product/edit/{id}', 'Admin\ProductController@update')->name('admin.product.update');
 Route::get('admin/product/delete/{id}', 'Admin\ProductController@delete')->name('admin.product.delete');
 Route::get('admin/all', 'Admin\ProductController@view')->name('admin.product.view');
Route::get('admin/product/barcode/{id}', 'Admin\ProductController@barcode')->name('admin.product.barcode');
});
//barcodes Routes
    Route::group(['prefix' => 'barcodes'], function(){
    Route::get('admin/index', 'Admin\BarcodeController@index')->name('admin.barcodes');
    Route::get('admin/create', 'Admin\BarcodeController@create')->name('admin.barcode.create');
    Route::get('admin/edit/{id}', 'Admin\BarcodeController@edit')->name('admin.barcode.edit');
    Route::post('admin/store', 'Admin\BarcodeController@store')->name('admin.barcode.store');
    Route::post('admin/barcode/edit/{id}', 'Admin\BarcodeController@update')->name('admin.barcode.update');
    Route::get('admin/barcode/delete/{id}', 'Admin\BarcodeController@delete')->name('admin.barcode.delete');
     Route::get('admin/barcode/set/{id}', 'Admin\BarcodeController@Barcode')->name('admin.barcode.set');
     Route::post('admin/barcode/store', 'Admin\BarcodeController@Barcodestore')->name('admin.barcode.store');


     Route::post('admin/barcode/view', 'Admin\BarcodeController@view')->name('admin.barcode.view');
    
     Route::get('admin/invoice/{id}', 'Admin\BarcodeController@General')->name('admin.barcode.invoice');
    
});


 

 // Customer  Controller

 //User  Customer  Controller


Route::get('/add/customer', 'CustomerController@customeradd')->name('add.customer');
Route::post('/insert/customer', 'CustomerController@customerinsert');
Route::get('/list/customer', 'CustomerController@customerview')->name('list.customer');
Route::get('/single/view/{cust_id}', 'CustomerController@custsingleview');
Route::get('/delete/customer/{cust_id}', 'CustomerController@customerdelete');
Route::get('/edit/customer/{cust_id}', 'CustomerController@customeredit');
Route::post('/edit/customer/update', 'CustomerController@customeredupdate');;

// Category Routes
Route::group(['prefix' => '/employees'], function(){
Route::get('/', 'EmployeeController@index')->name('employees');
Route::get('/create', 'EmployeeController@create')->name('employee.create');
Route::get('/edit/{id}', 'EmployeeController@edit')->name('employee.edit');
Route::post('/store', 'EmployeeController@store')->name('employee.store');
Route::post('/employee/edit/{id}', 'EmployeeController@update')->name('employee.update');
Route::get('/employee/delete/{id}', 'EmployeeController@delete')->name('employee.delete');
Route::get('/employee/view/{id}', 'EmployeeController@view')->name('employee.view');

});
//Admin Supplier Controller
Route::get('admin/add/supplier', 'SupplierController@supplieradd');
Route::post('/admin/insert/supplier', 'SupplierController@supplierinsert');
Route::get('/admin/list/supplier', 'SupplierController@supplierlist');
Route::get('/admin/delete/supplier/{sup_id}', 'SupplierController@supplierdelete');
Route::get('/admin/edit/supplier/{sup_id}', 'SupplierController@supplieredit');
Route::post('/admin/edit/supplier/update', 'SupplierController@supplierupdate');
Route::get('/admin/single/supplier/{sup_id}', 'SupplierController@suppliersingleview');

//Admin Customer  Controller
Route::get('/admin/list/customer', 'AdminController@admincustomerview')->name('admin.list.customer');

//Admin Branch  Controller
Route::get('/admin/add/branch', 'Admin\BranchController@addbranch')->name('admin.add.branch');
Route::post('/admin/insert/branch', 'Admin\BranchController@insertbranch')->name('admin.insert.branch');
Route::get('/admin/all/branch', 'Admin\BranchController@viewbranch')->name('admin.view.branch');
Route::get('/admin/delete/branch/{brance_id}', 'Admin\BranchController@deletebranch')->name('admin.delete.branch');
Route::get('/admin/edit/branch/{brance_id}', 'Admin\BranchController@editbranch')->name('admin.edit.branch');
Route::post('/admin/branch/update', 'Admin\BranchController@updatebranch')->name('admin.update.branch');

//Admin Bank  Controller
Route::group(['prefix' => 'admin'], function() {
Route::get('/add/bank', 'Admin\BankController@addbank')->name('admin.add.bank');
Route::post('/insert/bank', 'Admin\BankController@insertbank')->name('admin.insert.bank');
Route::get('/delete/bank/{bank_id}', 'Admin\BankController@deletebank')->name('admin.delete.bank');
Route::get('/edit/bank/{bank_id}', 'Admin\BankController@editbank')->name('admin.edit.bank');
Route::post('/bank/update', 'Admin\BankController@updatebank')->name('admin.update.bank');
// Route::get('/single/bank/{bank_id}', 'Admin\BankController@banksingleview')->name('admin.single.bank');
});

//Admin Bank  Controller
Route::group(['prefix' => 'admin'], function() {
Route::get('/add/bankholder', 'Admin\BankholderController@addbankholder')->name('admin.add.bankholder');
Route::post('/insert/bankholder', 'Admin\BankholderController@insertbank')->name('admin.insert.bankholder');
Route::get('/all/bankholder', 'Admin\BankholderController@viewbank')->name('admin.view.bankholder');
Route::get('/delete/bankholder/{bankholder_id}', 'Admin\BankholderController@deletebankholder')->name('admin.delete.bankholder');
Route::get('/edit/bankholder/{bankholder_id}', 'Admin\BankholderController@editbank')->name('admin.edit.bankholder');
Route::post('/bankholder/update', 'Admin\BankholderController@updatebank')->name('admin.update.bankholder');
Route::get('/single/bankholder/{bankholder_id}', 'Admin\BankholderController@bankholdersingleview')->name('admin.single.bankholder');
});



//Admin Deposit Controller
Route::group(['prefix' => 'admin'], function() {
Route::get('/add/deposit/bank', 'Admin\DepositController@adddeposit')->name('admin.add.deposit');
Route::post('/insert/deposit/bank', 'Admin\DepositController@insertdeposit')->name('admin.insert.deposit');

});
Route::get('get/holder/{bankid}','Admin\DepositController@GetHolder');
Route::get('get/details/holder/{hold_id}','Admin\DepositController@GetDetailsHolder');
Route::get('/get/pay/bank/{paybank_id}','Admin\DepositController@GetPaybank');
Route::get('/get/payment/bank/{payment_id}','Admin\DepositController@Pamentbank');



