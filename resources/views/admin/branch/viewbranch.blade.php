@extends('admin.admin_layouts')
@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->
      <!-- panel -->
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>All Branch</span>
          </div>
        </div>
        <div class="panel_body">        
            
            <div class="table-responsive">
              <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
                <thead>
                  <tr>                  
                    <th>Branch Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Password</th>
                    <th>Manager</th>
                    <th>Manager Phone</th>
                    <th>City</th>                    
                    <th>Zip Code</th>                    
                    <th>Action</th>                    
                  </tr>
                </thead>
                <tbody>
                  @foreach($branches as $branch)
                  <tr>
                    <td >{{$branch->name}}</td>                 
                    <td>{{$branch->email}}</td>
                    <td>{{$branch->phone}}</td>
                    <td>{{$branch->password}}</td>
                    <td>{{$branch->manager}}</td>
                    <td>{{$branch->manager_phone}}</td>
                    <td>{{$branch->city}}</td>
                    <td>{{$branch->zipcode}}</td>
                    <td>
                      <div class="btn-group" role="group">
                        <a href="{{url('/admin/delete/branch/'.$branch->id)}}" id="delete"  class="btn btn-sm btn-danger">Delete</a>
                        <a href="{{url('/admin/edit/branch/'.$branch->id)}}"  class="btn btn-sm btn-primary">Edit</a>
                      </div>                   
                    </td>   
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div> <!--/ panel body -->
            </div><!--/ panel -->
           
          </section>
          <!--/ page content -->
          <!-- start code here... -->
          </div><!--/middle content wrapper-->
          </div><!--/ content wrapper -->
          @endsection