@extends('admin.admin_layouts')
@section('admin_content')
<!-- content wrpper -->
<div class="content_wrapper">
  <!--middle content wrapper-->
  <!-- page content -->
  <div class="middle_content_wrapper">
    <section class="page_content">
      <!-- panel -->
      <!-- panel -->
      <div class="panel mb-0">
        <div class="panel_header">
          <div class="panel_title">
            <span class="panel_icon"><i class="fas fa-border-all"></i></span><span>All Supplier</span>
          </div>
        </div>
        <div class="panel_body">        
            
            <div class="table-responsive">
              <table id="dataTableExample1" class="table table-bordered table-striped table-hover mb-2">
                <thead>
                  <tr>
                    <th>Supplier ID</th>
                    <th>Supplier Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Mobile</th>
                    <th>Position</th>                    
                    <th>Bank Name</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($suppliers as $suplier)
                  <tr>
                    <td >{{$suplier->supplier_id}}</td>
                    <td>{{$suplier->name}}</td>
                    <td>{{$suplier->email}}</td>
                    <td>{{$suplier->address}}</td>
                    <td>{{$suplier->mobile}}</td>
                    <td>{{$suplier->position}}</td>
                    <td>{{$suplier->bank_name}}</td>
                    <td><img id="logo" src="{{asset('public/panel/assets/images/supplier/'.$suplier->image) }}" width="80" height="80px;" /></td>
                    <td>
                      <div class="btn-group" role="group">
                        <a href="{{url('/admin/single/supplier/'.$suplier->id)}}"  class="btn btn-sm btn-secondary">View</a>
                        <a href="{{url('/admin/delete/supplier/'.$suplier->id)}}" id="delete"  class="btn btn-sm btn-danger">Delete</a>
                        <a href="{{url('/admin/edit/supplier/'.$suplier->id)}}" class="btn btn-sm btn-primary">Edit</a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div> <!--/ panel body -->
            </div><!--/ panel -->
           
          </section>
          <!--/ page content -->
          <!-- start code here... -->
          </div><!--/middle content wrapper-->
          </div><!--/ content wrapper -->
          @endsection