<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Product;
use Image;
use File;
class ProductController extends Controller
{
     public function __construct()
  {
    $this->middleware('auth:admin');
  }

   public function index()
  {
    $products = Product::orderBy('id', 'desc')->get();
    return view('admin.product.index')->with('products', $products);
  }
   public function create()
   {
    return view('admin.product.create');
   }

   public function store(Request $request)
  {
      $request->validate([
            'product_name' => 'required|max:30',
            'category_id' => 'required|max:30',
            'brand_id' => 'required',
            'unit_id' => 'required',
            'buy_price' => 'required',
            'sell_price' => 'required',
            'vat' => 'required',
            'low_stock' => 'required',
            'up_stock' => 'required',
            'status' => 'required',
            'image' => 'nullable',
            
         ]); 
          $data=array();
          $data['product_code']= $request->product_code;
          $data['product_name']=$request->product_name;
          $data['category_id']=$request->category_id;
          $data['brand_id']=$request->brand_id;
          $data['unit_id']=$request->unit_id;
          $data['buy_price']=$request->buy_price;
          $data['sell_price']=$request->sell_price;
          $data['vat']=$request->vat;
          $data['low_stock']=$request->low_stock;
          $data['up_stock']=$request->up_stock;
          $data['barcode']=$request->product_code;
          $data['status']=$request->status;

          $image=$request->file('image');
           if ($image) {
            $image_name= hexdec(uniqid());
            $filename=$image_name.".".$image->getClientOriginalExtension();
            Image::make($image)->resize(200, 200)->save('public/panel/product/'.$filename);
            $data['image']=$filename;

            $product=DB::table('products')
                        ->insert($data);
         
          $notification=array(
            'messege'=>'product Added Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->route('admin.products')->with($notification);
        }else{
        	 $product=DB::table('products')
                        ->insert($data);
          $notification=array(
            'messege'=>'product successfull',
            'alert-type'=>'success'
             );
           return Redirect()->back()->with($notification);
          }

    }


 public function edit($id)
 {
      $product=DB::table('products')->where('id',$id)->first();
      return view('admin.product.edit',compact('product'));
 }
  public function update(Request $request,$id)
  {
      $validatedData = $request->validate([
            'product_name' => 'required|max:30',
            'category_id' => 'required|max:30',
            'brand_id' => 'required',
            'unit_id' => 'required',
            'buy_price' => 'required',
            'sell_price' => 'required',
            'vat' => 'required',
            'low_stock' => 'required',
            'up_stock' => 'required',
            'status' => 'required',
            'image' => 'nullable',
        
       ]);

        $data=array();
          $data['product_name']=$request->product_name;
          $data['product_code']=$request->product_code;
          $data['barcode']=$request->product_code;
          $data['category_id']=$request->category_id;
          $data['brand_id']=$request->brand_id;
          $data['unit_id']=$request->unit_id;
          $data['buy_price']=$request->buy_price;
          $data['sell_price']=$request->sell_price;
          $data['vat']=$request->vat;
          $data['low_stock']=$request->low_stock;
          $data['up_stock']=$request->up_stock;
          $data['status']=$request->status;
          $image=$request->file('image');
           if ($image) {
            $image_name= hexdec(uniqid());
            $filename=$image_name.".".$image->getClientOriginalExtension();
            Image::make($image)->resize(200, 200)->save('public/panel/product/'.$filename);
            $data['image']=$filename;
          $product=DB::table('products')->where('id',$id)->update($data);
          $notification=array(
            'messege'=>'product Update Successfully',
            'alert-type'=>'success'
             );
           return Redirect()->route('admin.products')->with($notification);
        }else{
        	 $product=DB::table('products')->where('id',$id)->update($data);
          $notification=array(
            'messege'=>'product successfull',
            'alert-type'=>'success'
             );
           return Redirect()->route('admin.products')->with($notification);
     }
       
    }



//deletye
     public function delete($id)   {

     $product = Product::find($id);
       if (!is_null($product)) {
        // Delete brand image
        if (File::exists('panel/product/'.$product->image)) {
          File::delete('panel/product/'.$product->image);
        }
        $product->delete();
      }
      return back();
      
     //      $product=DB::table('products')->where('id',$id)->first();
     //      $image=$product->image;
     //      $delete=DB::table('products')->where('id',$id)->delete();

     //       if ($delete) {
     //          unlink('public/panel/product/'.$image);
     //      $notification=array(
     //        'messege'=>'products Delete Successfully',
     //        'alert-type'=>'success'
     //         );
     //       return Redirect()->back()->with($notification);
     // }else{
     //     $notification=array(
     //        'messege'=>'Failed!',
     //        'alert-type'=>'error'
     //         );
     //       return Redirect()->back()->with($notification);
     }


     public function view()
     {
          $products=Product::orderBy('id', 'desc')->get();
       return view('admin.product.view',compact('products'));
     }

     public function barcode($id)
     {
       $product=DB::table('products')->where('id',$id)->first();
       return view('admin.barcode.create',compact('product'));
     }
   }


